/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solver;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Result 
{
    String nameResult;
    public int m, n;
    public double h;
    public double startTime, currentTime, endTime, del_t;
    int count;
    
    public double []x;
    public double []y;
    Map<String, ArrayList<double []>> data;
    ArrayList<String> nameColumn;
    int countIteratinos;
    StringBuffer sBuffer;// = new StringBuffer();
    double []tmpVector;
    
    public Result(String name)
    {
        nameResult = name;
        data = new HashMap<>();
        nameColumn = new ArrayList<>();
    }
    
    public void setVector(String name, double []collumn) //experemantal function
    {
        tmpVector = new double[n];
        System.arraycopy( collumn, 0, tmpVector, 0, n );
        
        boolean flag = true;
        for (String i : this.nameColumn)
        {
            if (i.equals(name))
            {
                flag = false;
                
                data.get(i).add(tmpVector);
                break;
            }
        }
        if (flag == true)
        {
            ArrayList<double []> tmpList = new ArrayList<>();
            data.put(name, tmpList);
            data.get(name).add(tmpVector);
            nameColumn.add(name);
            
        }
        
    }
    
    public double [] getVector(String nameVector)
    {
        return data.get(nameVector).get(0);
    }
            
    
    public void printToFile(String path)
    {
        FileWriter writer;
        try {
            writer = new FileWriter(path, false);
            writer.write(assemblyDataFile().toString());
            writer.close();
        } catch (IOException ex) {
            System.out.println("errorCreateFile = " + ex);
        }
    }
    
    public StringBuffer assemblyDataFile()
    {
        sBuffer = new StringBuffer();
        
        String tmp = "";
        
        countIteratinos = (int)(endTime / del_t);
        if (countIteratinos == 0)
        {
            countIteratinos = 1;
        }
        
        sBuffer.append("TITLE=\"" + nameResult + "\"\n");
        sBuffer.append("VARIABLES=x, y");
        for (String k : this.nameColumn)
        {
            sBuffer.append(", " + k);
        }
        sBuffer.append("\n");
        
        while (count < countIteratinos)
        {      
            tmp += "ZONE T=\"" + currentTime + "\", I=" + m + ", J = " + m + ",F=POINT\n";
            
            sBuffer.append(tmp);
            tmp = "";

            for (int i = 0; i<m; i++)
            {
                for (int j = 0; j<m; j++)
                {
                    tmp += x[j] + "  " + y[i];
                    for (String k : this.nameColumn)
                    {
                        try
                        {
                            tmp += " " + data.get(k).get(count)[i * m + j];
                        }catch(IndexOutOfBoundsException e)
                        {
                            tmp += " " + data.get(k).get(0)[i * m + j];
                        }
                    }
                    tmp += "\n";
                    sBuffer.append(tmp);
                    tmp = "";
                }
            }
            count++;
            currentTime += del_t;
            tmp += "\n\n";
            sBuffer.append(tmp);
            tmp = "";
        }
        return sBuffer;
    }
    
}
