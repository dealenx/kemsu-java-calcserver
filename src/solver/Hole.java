package solver;

public class Hole {

    public int number;
    public double y0;
    public double y1;
    public double u;
    public int flag;

    public Hole(int n, double yy0, double yy1, double uu, int flagg) {
        number = n;
        y0 = yy0;
        y1 = yy1;
        u = uu;
        flag = flagg;
    }
}
