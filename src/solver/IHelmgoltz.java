/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solver;

import java.util.ArrayList;

/**
 *
 * @author 1
 */
public interface IHelmgoltz {

    public void setData(int n, double eps, double k);

    public void solve();

    public Result getResult();

    public void setHole(ArrayList<Hole> hole);
}
