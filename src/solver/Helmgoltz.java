/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solver;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author 1
 */
public class Helmgoltz implements IHelmgoltz {

    private double eps;
    private double h;
    private int m ,n;
    private double k;
    private int iterations;
    private final String name;
    private Result res;
    private ArrayList<Hole> hole;
    private int countHole;

    private double[] x;
    private double[] y;
    private double[][] f;
    private double[][] psiPrev;
    private double[][] psiNext;
    private double[][] u;
    private double[][] v;
    private double[][] R;
    private double[][] Ar;
    private double[][] Au;

    public Helmgoltz(String name) {
        this.name = name;
    }

    @Override
    public void setData(int m, double eps, double k) {
        this.m = m + 1; //количество узлов на границе
        this.eps = eps;
        this.k = k;

        init();

    }

    @Override
    public void solve() {
        checkBorderConditions();
        createMesh();
        fillRightPart();
        calc();
        saveResult();
    }

    private void checkBorderConditions() {
        float flowIn = 0, flowOut = 0;
        for (int i = 0; i < countHole; i++) {
            if (hole.get(i).flag == 0) {
                flowIn += (hole.get(i).y1 - hole.get(i).y0) * hole.get(i).u;
            } else {
                flowOut += (hole.get(i).y1 - hole.get(i).y0) * hole.get(i).u;
            }
        }

        //System.out.println("in = " + flowIn);
        //System.out.println("out = " + flowOut);
        if (flowIn > flowOut) {
            throw new NullPointerException("(flowIn > flowOut).\nFlow in = " + flowIn + "\nFlow out = " + flowOut);
        } else if (flowIn < flowOut) {
            throw new NullPointerException("(flowOut > flowIn).\nFlow in = " + flowIn + "\nFlow out = " + flowOut);
        }
    }

    private void calc() {
        double Rn = 1;
        double tau;
        setPsi(psiPrev);
        setPsi(psiNext);
        operator(Au, psiPrev);
        findR();

        while (Rn > eps) {
            iterations++;
            operator(Ar, R);
            tau = findTau();

            for (int i = 1; i < m - 1; i++) {
                for (int j = 1; j < m - 1; j++) {
                    psiNext[i][j] = psiPrev[i][j] - tau * R[i][j];
                }
            }

            psiPrev = psiNext;

            operator(Au, psiPrev);
            findR();
            Rn = maxElement(R);
        }
        findUV();
    }

    private void saveResult() {
        res.nameResult = name;
        res.x = this.x;
        res.y = this.y;
        res.h = this.h;
        res.m = this.m;
        res.n = (this.m  * this.m);
        
        double []tmp = new double[res.n];
        
        for (int i = 0; i<res.m;i++)
        {
            for (int j = 0; j<res.m;j++)
            {
                tmp[i * res.m + j] = psiNext[i][j];
            }
        }
        
        res.setVector("psi", tmp);
        
        for (int i = 0; i<res.m;i++)
        {
            for (int j = 0; j<res.m;j++)
            {
                tmp[i * res.m + j] = u[i][j];
            }
        }
        res.setVector("u", tmp);
        for (int i = 0; i<res.m;i++)
        {
            for (int j = 0; j<res.m;j++)
            {
                tmp[i * res.m + j] = v[i][j];
            }
        }
        res.setVector("v", tmp);
    }

    private void init() {
        hole = new ArrayList<>();
        res = new Result(name);

        h = 1. / (m - 1);
        n = m * m;
        iterations = 0;

        x = new double[m];
        y = new double[m];
        f = new double[m][m];
        psiPrev = new double[m][m];
        psiNext = new double[m][m];
        u = new double[m][m];
        v = new double[m][m];
        R = new double[m][m];
        Ar = new double[m][m];
        Au = new double[m][m];

        countHole = 0;
    }

    private void fillRightPart() {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                f[i][j] = pow(k, 2) * y[j];
            }
        }
    }

    private void operator(double[][] res, double[][] u) {
        double a, b, c;
        for (int i = 1; i < m - 1; i++) {
            for (int j = 1; j < m - 1; j++) {
                a = (u[i + 1][j] - 2 * u[i][j] + u[i - 1][j]) / pow(h, 2.0);
                b = (u[i][j + 1] - 2 * u[i][j] + u[i][j - 1]) / pow(h, 2.0);
                c = pow(k, 2) * u[i][j];
                res[i][j] = a + b + c;
            }
        }
    }

    private void findR() {
        for (int i = 1; i < m - 1; i++) {
            for (int j = 1; j < m - 1; j++) {
                R[i][j] = Au[i][j] - f[i][j];
            }
        }
    }

    private double findTau() {
        double t1 = scalar(Ar, R);
        double t2 = scalar(Ar, Ar);
        return t1 / t2;
    }

    private double maxElement(double[][] a) {
        double max = 0.0;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                if (abs(a[i][j]) > max) {
                    max = abs(a[i][j]);
                }
            }
        }
        return max;
    }

    private void findUV() {
        for (int i = 1; i < m - 1; i++) {
            for (int j = 1; j < m - 1; j++) {
                u[i][j] = (psiNext[i + 1][j] - psiNext[i][j]) / h;
            }
        }

        for (int i = 1; i < m - 1; i++) {
            for (int j = 1; j < m - 1; j++) {
                v[i][j] = -(psiNext[i][j + 1] - psiNext[i][j]) / h;
            }
        }
    }

    private void setPsi(double[][] psi) {
        //int nHole = 2;
        //Hole[] H = new Hole[nHole];

        //H[0]=new Hole(1,0.4,0.6,1,0);
        //H[1]=new Hole(1,0.7,0.8,0.5,0);
        //H[1]=new Hole(3,0.4,0.6,1,1);
        double sumU = 0.0;
        for (int i = 0; i < countHole - 1; i++) {
            sumU += pow(-1, hole.get(i).flag) * hole.get(i).u * abs(hole.get(i).y1 - hole.get(i).y0);//y0-y1
        }
        sumU *= (-1.0 / abs(hole.get(countHole - 1).y1 - hole.get(countHole - 1).y0));
        if (sumU > 0) {
            hole.get(countHole - 1).flag = 0;
        } else {
            hole.get(countHole - 1).flag = 1;
        }

        hole.get(countHole - 1).u = abs(sumU);
        double C = 0.0;
        for (int i = 0; i < m; i++) {
            psi[i][0] = C;
            for (int j = 0; j < countHole; j++) {
                if (hole.get(j).number == 1) {
                    if (abs(y[i] - hole.get(j).y0) <= h / 2) {
                        C = C - hole.get(j).u * pow(-1, hole.get(j).flag) * y[i];
                        psi[i][0] = hole.get(j).u * pow(-1, hole.get(j).flag) * y[i] + C;
                    }
                    if ((hole.get(j).y0 <= y[i]) && (y[i] <= hole.get(j).y1)) {
                        psi[i][0] = hole.get(j).u * pow(-1, hole.get(j).flag) * y[i] + C;
                    }
                    if (abs(y[i] - hole.get(j).y1) <= h / 2) {
                        psi[i][0] = hole.get(j).u * pow(-1, hole.get(j).flag) * y[i] + C;
                        C = psi[i][0];
                    }
                }
            }
        }

        for (int i = 0; i < m; i++) {
            psi[m - 1][i] = C;
            for (int j = 0; j < countHole; j++) {
                if (hole.get(j).number == 2) {
                    if (abs(x[i] - hole.get(j).y0) <= h / 2) {
                        C = C + hole.get(j).u * pow(-1, hole.get(j).flag + 1) * x[i];
                        psi[m - 1][i] = -hole.get(j).u * pow(-1, hole.get(j).flag + 1) * x[i] + C;
                    }

                    if ((hole.get(j).y0 <= x[i]) && (x[i] <= hole.get(j).y1)) {
                        psi[m - 1][i] = -hole.get(j).u * pow(-1, hole.get(j).flag + 1) * x[i] + C;
                    }

                    if (abs(x[i] - hole.get(j).y1) <= h / 2) {
                        psi[m - 1][i] = -hole.get(j).u * pow(-1, hole.get(j).flag + 1) * x[i] + C;
                        C = psi[m - 1][i];
                    }
                }
            }
        }
        C = 0;

        for (int i = 0; i < m; i++) {
            psi[0][i] = C;
            for (int j = 0; j < countHole; j++) {
                if (hole.get(j).number == 0) {
                    if (abs(y[i] - hole.get(j).y0) <= h / 2) {
                        C = C + hole.get(j).u * pow(-1, hole.get(j).flag) * x[i];
                        psi[0][i] = -hole.get(j).u * pow(-1, hole.get(j).flag) * x[i] + C;
                    }
                    if ((hole.get(j).y0 <= x[i]) && (x[i] <= hole.get(j).y1)) {
                        psi[0][i] = -hole.get(j).u * pow(-1, hole.get(j).flag) * x[i] + C;
                    }
                    if (abs(x[i] - hole.get(j).y1) <= h / 2) {
                        psi[0][i] = -hole.get(j).u * pow(-1, hole.get(j).flag) * x[i] + C;
                        C = psi[0][i];
                    }
                }
            }
        }

        for (int i = 0; i < m; i++) {
            psi[i][m - 1] = C;
            for (int j = 0; j < countHole; j++) {
                if (hole.get(j).number == 3) {
                    if (abs(y[i] - hole.get(j).y0) <= h / 2) {
                        C = C - hole.get(j).u * pow(-1, hole.get(j).flag + 1) * y[i];
                        psi[i][m - 1] = hole.get(j).u * pow(-1, hole.get(j).flag + 1) * y[i] + C;
                    }
                    if ((hole.get(j).y0 <= y[i]) && (y[i] <= hole.get(j).y1)) {
                        psi[i][m - 1] = hole.get(j).u * pow(-1, hole.get(j).flag + 1) * y[i] + C;
                    }
                    if (abs(y[i] - hole.get(j).y1) <= h / 2) {
                        psi[i][m - 1] = hole.get(j).u * pow(-1, hole.get(j).flag + 1) * y[i] + C;
                        C = psi[i][m - 1];
                    }
                }
            }
        }
    }

    private void createMesh() {
        for (int i = 0; i < m; i++) {
            x[i] = i * h;
            y[i] = i * h;
        }
    }

    private void nullArray(double[][] a) {
        for (int i = 0; i < m; i++) {
            Arrays.fill(a[i], 0);
        }
    }

    private double scalar(double[][] a, double[][] b) {
        double sum = 0.0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                sum += h * h * a[i][j] * b[i][j];
            }

        }
        return sum;
    }

    @Override
    public void setHole(ArrayList<Hole> hole) {
        this.hole = hole;
        countHole = hole.size();
    }

    @Override
    public Result getResult() {
        return res;
    }
}
