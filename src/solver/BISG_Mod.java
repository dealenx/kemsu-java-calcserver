/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solver;

import static java.lang.Math.*;

/**
 *
 * @author konstantin
 */
public class BISG_Mod 
{
    private void zero(double []vector, int n)
    {
        for (int i =0;i<n;i++)
        {
            vector[i] = 0;
        }
        
    }
    
    private double scalar(double []a, double []b, int n)
    {
        double sum = 0;
        for (int i =0;i<n;i++)
        {
            sum += a[i] * b[i];
        }
        return sum;
    }
    
    int calc(double [][] AA, double [] B, double [] X, int n)
    {
        double []Rn;
    double []_Rn;
    double []Pn;
    double []Pn1;
    double []Vn;
    double []A;
    double eps = 10e-10;
    double []Sn;
    double []Tn;
    int iter = 0;
    Rn = new double[n];
    Pn = new double[n];
    Pn1 = new double[n];
    _Rn = new double[n];
    Vn = new double[n];
    A = new double[n];
    Sn = new double[n];
    Tn = new double[n];

    zero(A, n);

    for (int i = 0; i < n; i++)
    {
        X[i] = B[i] + 0.000001;
    }


    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            A[i] += AA[i][j] * X[j];  //������������ � �� 1�� ����������� ������� �=1
        }
        _Rn[i] = Rn[i] = B[i] - A[i];
    }

    zero(Pn, n);
    zero(Vn, n);

    double pn = 1.;
    double an = 1.;
    double bn = 1.;
    double wn = 1.;
    double pn1;
    double norm = 0;

    int p = 1;
    while (true)
    {
        pn1 = scalar(_Rn, Rn, n);
        //
        if (pn1 == 0)
        {
            //cout << "ERROR!" << endl;
        }
        if (p == 1)
        {
            for (int i = 0; i < n; i++)
            {
                Pn1[i] = Rn[i];
            }
        }
        else
        {
            bn = (pn1 / pn)*(an / wn);
            for (int i = 0; i < n; i++)
            {
                Pn1[i] = Rn[i] + bn * (Pn[i] - wn * Vn[i]);
            }
        }
        //
        zero(Vn, n);
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Vn[i] += AA[i][j] * Pn1[j];
            }
        }

        an = pn1 / scalar(_Rn, Vn, n);
        for (int i = 0; i < n; i++)
        {
            Sn[i] = Rn[i] - an * Vn[i];
        }
        if ((sqrt(scalar(Sn, Sn, n))) <= eps)
        {
            for (int i = 0; i < n; i++)
            {
                X[i] += an * Pn1[i];
            }
            //cout << "iter = " << iter << endl;

            return 1;
        }
        else
        {
            zero(Tn, n);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Tn[i] += AA[i][j] * Sn[j];
                }
            }

            wn = scalar(Tn, Sn, n) / scalar(Tn, Tn, n);
            for (int i = 0; i < n; i++)
            {
                X[i] += an * Pn1[i] + wn * Sn[i];
                Rn[i] = Sn[i] - wn * Tn[i];
            }

            pn = pn1;
            for (int i = 0; i < n; i++)
            {
                Pn[i] = Pn1[i];
            }

            if ((sqrt(scalar(Rn, Rn, n))) <= eps)
            {

                return 1;
            }
        }
        p++;
        iter++;
    }
    }
    
    public BISG_Mod(double [][] AA, double [] B, double [] X, int n)
    {
        calc(AA, B, X, n);
    }
}
