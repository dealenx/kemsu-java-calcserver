package calc_server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Calc_server {

    public static DataBaseConnect db;

    public static void main(String[] args) throws IOException, InterruptedException {
        db = new DataBaseConnect();
        run();
    }

    public static void run() throws IOException, InterruptedException {
        boolean temp_status = false;
        db.connect();
        ArrayList tempArrayList;
        while (true) {
            temp_status = db.checkUnfinishedTasks();
            System.out.println("status: " + temp_status);
            if (temp_status) {
                ServerThread server = new ServerThread(db);
                server.start();//запуск потока
            }
            Thread.sleep(1000);
        }
    }

}
