/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calc_server;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author dealenx
 * 
 * Клиент для тестирования сервера
 * 
 */


public class Сalc_client {

    public static DataBaseConnect db;

    public static void main(String[] args) throws IOException, InterruptedException {
        db = new DataBaseConnect();
        db.connect();
        sendDataAB(ThreadLocalRandom.current().nextInt(1, 10 + 1), ThreadLocalRandom.current().nextInt(1, 10 + 1));
        sendDataABC(ThreadLocalRandom.current().nextInt(1, 10 + 1), ThreadLocalRandom.current().nextInt(1, 10 + 1), ThreadLocalRandom.current().nextInt(1, 10 + 1));
        db.disConnect();
    }

    public static void sendDataAB(int a, int b) {
        try {
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();
            String type_calc = "simple";
            String name = "simpleNAME";
            int user_id = 1;
            String res = "";
            boolean statusready = false;
            boolean calcing_status = false;

            String args
                    = "{"
                    + "a: " + a + ","
                    + "b: " + b
                    + "}";
            String data = "INSERT INTO \"tasks\" ( guid, name, typecalc, userid, resdata, statusready, args, calcing_status) "
                    + "VALUES ( \'" + randomUUIDString  + "\', \'" + name + "\', \'" + type_calc + "\', " + user_id + ", \'" + res + "\', " + statusready + ", \'" + args + "\'" + "," + calcing_status + "" + " );";
            db.statement.executeUpdate(data);
        } catch (Exception e) {
            System.out.println("Error write data base: " + e);
        }
    }

    public static void sendDataABC(int a, int b, int c) {
        try {
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();
            String type_calc = "simpleABC";
            String name = "simpleABCNAME";
            int user_id = 1;
            String res = "";
            boolean statusready = false;
            boolean calcing_status = false;

            String args
                    = "{"
                    + "a: " + a + ","
                    + "b: " + b + ","
                    + "c: " + c
                    + "}";
            String data = "INSERT INTO \"tasks\" ( guid, name, typecalc, userid, resdata, statusready, args, calcing_status) "
                    + "VALUES ( \'" + randomUUIDString  + "\', \'" + name + "\', \'" + type_calc + "\', " + user_id + ", \'" + res + "\', " + statusready + ", \'" + args + "\'" + "," + calcing_status + "" + " );";
            db.statement.executeUpdate(data);
        } catch (Exception e) {
            System.out.println("Error write data base: " + e);
        }
    }
}
