/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calc_server;

import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author dealenx
 */

/*
-- Table: public.tasks

-- DROP TABLE public.tasks;

CREATE TABLE public.tasks
(
  guid text,
  typecalc text,
  userid integer,
  resdata text,
  statusready boolean,
  args text,
  calcing_status boolean
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tasks
  OWNER TO postgres;
 */
public class DataBaseConnect {

    private String url, login, password;
    public Connection con;
    public Statement statement;
    private ResultSet rs;

    public String infoError;
    private int countRows;

    public DataBaseConnect() {
        System.out.println("DataBaseConnect()");
        init();

    }

    private void init() {
        this.url = "jdbc:postgresql://localhost:5432/jsfbd";
        this.login = "postgres";
        this.password = "1234";
        this.countRows = 0;
    }

    public void connect() {
        try {
            Class.forName("org.postgresql.Driver");
            this.con = DriverManager.getConnection(this.url, this.login, this.password);
            System.out.println("CONNECTED TO DB");
            try {
                this.statement = this.con.createStatement();
                System.out.println("DB STATEMENT");
            } catch (Exception e) {
                this.infoError = "Error create statment: " + e;
            }

        } catch (Exception e) {

            this.infoError = "Error connect to data base: " + e;
        }

    }

    public void disConnect() {
        System.out.println("disConnect()");
        try {
            rs.close();

            try {
                statement.close();

                try {
                    con.close();
                } catch (Exception e) {
                    infoError = "Error disconnect data base: " + e;
                }

            } catch (Exception e) {

                infoError = "Error statement close: " + e;
            }

        } catch (Exception e) {

            infoError = "Error close tables: " + e;
        }
    }

    public void updateCalcingStatus(String guid) {
        try {
            this.statement = this.con.createStatement();
            String sql = "UPDATE \"tasks\" "
                    + "SET calcing_status =  true "
                    + "WHERE guid = '" + guid + "' ;";
            this.statement.executeUpdate(sql);
        } catch (Exception e) {

            System.out.println("Error write data base: " + e);
            this.infoError = "Error write data base: " + e;
        }
    }

    public ArrayList getLastUnfinishedTask() {
        System.out.println("getLastUnfinishedTask()  ");
        ArrayList tempArrayList = new ArrayList();
        boolean status = false;
        try {
            System.out.println("getLastUnfinishedTask() try-section ");
            String sql = "SELECT guid, typecalc, userid, resdata, statusready, args, calcing_status FROM \"tasks\" WHERE calcing_status = false and statusready = false ORDER BY guid DESC LIMIT 1 ; ";
            this.rs = this.statement.executeQuery(sql);

            while (this.rs.next()) {
                tempArrayList.add(this.rs.getString("guid"));
                tempArrayList.add(this.rs.getString("typecalc"));
                tempArrayList.add(this.rs.getString("userid"));
                tempArrayList.add(this.rs.getBoolean("statusready"));
                tempArrayList.add(this.rs.getBoolean("calcing_status"));
                tempArrayList.add(this.rs.getString("args"));
            }

        } catch (Exception e) {

            this.infoError = "Error get count rows: " + e;
        }

        //System.out.println("tmp: " + temp);
        return tempArrayList;
    }

    public boolean checkUnfinishedTasks() {
        int temp_count = -1;
        boolean status = false;
        try {
            String sql = "SELECT count(guid) as count_statusready FROM \"tasks\" WHERE calcing_status = false and statusready = false;  ";
            this.rs = this.statement.executeQuery(sql);
            //this.rs.next();
            while (this.rs.next()) {
                temp_count = this.rs.getInt("count_statusready");
            }

            if (temp_count > 0) {
                status = true;
            } else {
                status = false;
            }
        } catch (Exception e) {
            this.infoError = "Error get count rows: " + e;
        }
        return status;
    }

    public void updateResult(String guid, String result_data) {
        try {
            this.statement = this.con.createStatement();
            String sql = "UPDATE \"tasks\" "
                    + " SET resdata = '" + result_data + "' " + ", " + " statusready = true "
                    + " WHERE guid = '" + guid + "' ;";
            this.statement.executeUpdate(sql);
        } catch (Exception e) {

            System.out.println("Error write data base: " + e);
            this.infoError = "Error write data base: " + e;
        }
    }

}
