package calc_server;

import static calc_server.Calc_server.db;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import solver.*;

class ServerThread extends Thread {

    private DataBaseConnect db;

    // Путь к папке, где будут сохранятья расчеты 
    public static String path_data_folder = "/home/dealenx/dev/calc_server/data/";

    public ServerThread(DataBaseConnect db) throws IOException {
        this.db = db;
    }

    public void run() {
        boolean temp_status;

        ArrayList tempArrayList;

        tempArrayList = db.getLastUnfinishedTask();
        System.out.println(tempArrayList.get(0) + "");
        String UUIDString = tempArrayList.get(0).toString();
        String calc_case = tempArrayList.get(1).toString();
        String calc_args = tempArrayList.get(5).toString();
        db.updateCalcingStatus(UUIDString);
        String path_data_file = path_data_folder + UUIDString + ".dat";
        System.out.println("calc_args: " + calc_args);
        choiceOfCalc(path_data_file, calc_case, calc_args);

        boolean status_ready_result_data = false;
        String dataFromFile = "";

        File f = new File(path_data_file);

        // Проверка существование файла результатов
        while (status_ready_result_data != true) {
            System.out.println(f + (f.exists() ? " is found " : " is missing "));

            if (f.exists()) {
                System.out.println("file: true");
                status_ready_result_data = true;
            } else {
                System.out.println("file: false");
                status_ready_result_data = false;
            }
        }

        if (status_ready_result_data) {
            try {
                dataFromFile = readUsingBufferedReader(path_data_file);
            } catch (IOException e) {
                System.out.println("exception happened - here's what I know: ");
                e.printStackTrace();
            }

            db.updateResult(UUIDString, dataFromFile);
        }

    }

    public void choiceOfCalc(String path_file, String calc_case, String args) {
        switch (calc_case) {
            case "simple":
                calcSimpleAB(path_file, calc_case, args);
                break;
            case "helmholtz":
                calcHelmholtz(path_file, calc_case, args);
                break;
            case "transportsub":
                calcTransportSub(path_file, calc_case, args);
                break;
            case "simpleABC":
                calcSimpleABC(path_file, calc_case, args);
                break;
            default:
                System.out.println("ERROR CASE OF TYPE CALC");
                break;
        }
    }//calcSimpleTransportSub
    
    public void calcHelmholtz(String path_file, String calc_case, String args) {
        // Путь к c++ программе 
        String path_program = "/home/dealenx/dev/calc_server/cpp/run.exe";

        Gson gson = new Gson();
        JsonElement elementJMessage = new JsonParser().parse(args);
        JsonObject objectJMessage = elementJMessage.getAsJsonObject();

        String k = objectJMessage.get("k").getAsString();
        String eps = objectJMessage.get("eps").getAsString();
        String name = "test";
        
        JsonArray jsonArray = objectJMessage.getAsJsonArray("holes");
        TypeToken<List<Hole>> token = new TypeToken<List<Hole>>() {};
        List<Hole> holes = gson.fromJson(jsonArray, token.getType());
        
        System.out.println("====================");
        
        System.out.println("k: " + k);
        System.out.println("eps: " + eps);
        System.out.println("Hole[0].y1: " + holes.get(0).y1);
        
        
        System.out.println("====================");
        
        int m = 20;
        Result res;
        IHelmgoltz ih = new Helmgoltz(name);
        ih.setData(20, Double.valueOf(eps), Double.valueOf(k));
        
        ih.setHole((ArrayList<Hole>) holes);
        
        try
        {
            ih.solve();
            res = ih.getResult();
            res.printToFile(path_file);
            
        } catch (Exception e)
        {
            System.out.println("solve = " + e);
        }
    }
    
    public void calcTransportSub(String path_file, String calc_case, String args) {
        // Путь к c++ программе 
        String path_program = "/home/dealenx/dev/calc_server/cpp/runABC.exe";

        Gson gson = new Gson(); 
        JsonElement elementJMessage = new JsonParser().parse(args);
        JsonObject objectJMessage = elementJMessage.getAsJsonObject();

        String a = objectJMessage.get("m").getAsString();
        String b = objectJMessage.get("d").getAsString();
        String c = objectJMessage.get("d").getAsString();

        String command = path_program + " " + a + " " + b + " " + c + " " + path_file;

        try {
            Process p = Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            System.out.println("exception happened - here's what I know: ");
            e.printStackTrace();
        }
    }

    public void calcSimpleAB(String path_file, String calc_case, String args) {
        // Путь к c++ программе 
        String path_program = "/home/dealenx/dev/calc_server/cpp/run.exe";

        Gson gson = new Gson();
        JsonElement elementJMessage = new JsonParser().parse(args);
        JsonObject objectJMessage = elementJMessage.getAsJsonObject();

        String a = objectJMessage.get("a").getAsString();
        String b = objectJMessage.get("b").getAsString();

        String command = path_program + " " + a + " " + b + " " + path_file;

        try {
            Process p = Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            System.out.println("exception happened - here's what I know: ");
            e.printStackTrace();
        }
    }

    public void calcSimpleABC(String path_file, String calc_case, String args) {
        // Путь к c++ программе 
        String path_program = "/home/dealenx/dev/calc_server/cpp/runABC.exe";

        Gson gson = new Gson();
        JsonElement elementJMessage = new JsonParser().parse(args);
        JsonObject objectJMessage = elementJMessage.getAsJsonObject();

        String a = objectJMessage.get("a").getAsString();
        String b = objectJMessage.get("b").getAsString();
        String c = objectJMessage.get("c").getAsString();

        String command = path_program + " " + a + " " + b + " " + c + " " + path_file;

        try {
            Process p = Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            System.out.println("exception happened - here's what I know: ");
            e.printStackTrace();
        }
    }
    
    

    public void checkerFile(String path_data_file, boolean status_ready_result_data) {
        File f = new File(path_data_file);

        while (status_ready_result_data != true) {
            System.out.println(f + (f.exists() ? " is found " : " is missing "));

            if (f.exists()) {
                System.out.println("file: true");
                status_ready_result_data = true;
            } else {
                System.out.println("file: false");
                status_ready_result_data = false;
            }
        }
    }

    private String readUsingBufferedReader(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }

        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }
}
