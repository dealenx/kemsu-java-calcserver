#include <iostream>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
//stringstream str;


int main(int argc, char* argv[]) {
    stringstream str;
    double a = 0, b = 0, c = 0;
    string path_file = "none.txt";
    cout << "args" << endl;

    string full_arg = "";

    for(int i =1; i < argc; i++) {
        full_arg.append(argv[i]);
        full_arg.append(" ");
    }


    str << full_arg; 
    str >> a >> b >> c >> path_file; 
    cout << "vars: " << endl;

    cout << a << endl;
    cout << b << endl;
    cout << path_file << endl;

    double res =  a + b + c;
    ofstream myfile; 
    myfile.open (path_file);
    myfile << "res" << endl;
    myfile << res << endl;
    myfile.close();
    return 0;
}

